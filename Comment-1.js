'use strict';

const comment = {
    date: new Date(),
    text: 'Datos necesarios para la carga de dato!',
    author: {
        name: 'Nombre sucursal IP: 198.12.0.223',
        avatarUrl: 'http://placekitten.com/g/64/61'
    }
};

function formatDate(date) {
    return date.toLocaleDateString();
}

function Avatar(parametros) {
    return (
        <img className="Avatar" 
        src = { parametros.valorP.avatarUrl }
        alt = { parametros.valorP.name } />
    );
}

function UserInfo(parametros) {
    return (
        <div className="UserInfo">
            <Avatar valorP = { parametros.author } />
            <div className = "authorInfo-name">
                { parametros.author.name }
            </div>
        </div>
    );
}

function Comment(parametros) {
    return (
        <div className="Comment">
            <UserInfo author = { parametros.author } />
            <div className = "Comment-text">
                { parametros.text }
            </div>
            <div className = "Comment-date">
                { formatDate(parametros.date) }
            </div>
        </div>
    );
}

ReactDOM.render(
    <Comment 
        date = { comment.date }
        text = { comment.text }
        author = { comment.author } />,
    document.getElementById('root')
);